var _STRINGS = {
	"Ad":{
		"Mobile":{
			"Preroll":{
				"ReadyIn":"The game is ready in ",
				"Loading":"Your game is loading...",
				"Close":"Close",
			},
			"Header":{
				"ReadyIn":"The game is ready in ",
				"Loading":"Your game is loading...",
				"Close":"Close",
			},
			"End":{
				"ReadyIn":"Advertisement ends in ",
				"Loading":"Please wait ...",
				"Close":"Close",
			},								
		},
	},
	
	"Splash":{
		"Loading":"Loading ...",	
		"LogoLine1":"Some text here",
		"LogoLine2":"powered by MarketJS",
		"LogoLine3":"none", 
        "TapToStart":"TAP TO START", 
	},

	"Game":{
		"btnPlay":"Jouer",
		"question":"Question "
	},

	"Results":{
		"bravo1":"WELL DONE!",
		"bravo2":"WELL DONE!",
		"result1":"You answered ",
		"result2":" of ",
		"result3":" questions correctly."
	},	

};

//"textAnswer":is all answers will appear in game.
//textAnswer number "1": is the right answer.
//"questions":is the questions will appear in game.
var _Question={
	size:{x:420,y:430}, //size box answer
	marginCorrectAnswer:4,// correct >= margincorrectanswer
	
	//EASY
	1:{
		"question":"Which famous action star became governor of California?",
		"description":"Arnold Schwarzenegger won the recall election in 2003 and was re-elected to serve a full term until 2011.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Arnold Schwarzenegger",
			"2":"Dwayne Johnson",
			"3":"Sylvester Stallone",
			"4":"Tom Cruise",
		},
	},
	2:{
		"question":"When was the first time that recession was recorded in America?",
		"description":"While recessions have happened since the 1800's, the first recorded recessions to have happened were around the 1920's.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"1920's",
			"2":"1930's",
			"3":"1940's",
			"4":"1950's",
		}
	},
	3:{
		"question":"The Cold War was between which two countries?",
		"description":"The Cold War referred to the tension between two superpowers, namely: Soviet Union and USA.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Soviet Union and USA",
			"2":"China and Japan",
			"3":"Russia and France",
			"4":"China and USA",
		}
	},
	4:{
		"question":"How did the Iran-Iraq War of 1980 end?",
		"description":"The Iran-Iraq War ended when Iran accepted a ceasefire from the UN after Iran lost the war.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"UN brokered a ceasefire",
			"2":"Iran lost the war",
			"3":"Iraq lost the war",
			"4":"Both parties gave up",
		}
	},
	5:{
		"question":"When was the start of World War I?",
		"description":"The official declaration of war was made by Austria-Hungary on July 28, 1914.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"July 28, 1914",
			"2":"September 1, 1939",
			"3":"June 14, 1890",
			"4":"December 2, 1916",
		}
	},
	6:{
		"question":"When was the start of World War II?",
		"description":"Adolf Hitler invaded Poland on September 1, 1939. which sparked the Second World War.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"September 1, 1939",
			"2":"November 11, 1940",
			"3":"December 30, 1899",
			"4":"October 2, 1920",
		}
	},
	7:{
		"question":"What is the name of the last King of France?",
		"description":"Louis XVI was the last King of France before the French Revolution happened which caused the monarchy system to collapse.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Louis XVI",
			"2":"Louis XV",
			"3":"Louis III",
			"4":"Louis XIV",
		}
	},
	8:{
		"question":"The battle between the House of Plantagenet and House of Valois is called as...",
		"description":"The Hundred Years' War happened between 1337 to 1453 between the two Houses of England and France.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Hundred Years' War",
			"2":"Civil War",
			"3":"World War I",
			"4":"Anglo-American War",
		}
	},
	9:{
		"question":"The WHO stands for...",
		"description":"The World Health Organization falls under the United Nations' system and is responsible for international public health.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"World Health Organization",
			"2":"World History Orientation",
			"3":"World Holy Ornament",
			"4":"World Historian Opportunity",
		}
	},
	10:{
		"question":"Who was the founder of Red Cross?",
		"description":"Clara Barton established the Red Cross in 1881 as means of providing humanitarian aid to victims of war.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Clara Barton",
			"2":"Richard Gordon",
			"3":"Gail McGovern",
			"4":"Gerard Anderson",
		}
	},
	11:{
		"question":"Gold and other resources allegedly stolen and hidden by the Japanese from Southeast Asia was called...",
		"description":"Yamashita's Treasure is considered as an urban legend regarding caves and underground dwellings containing gold left by Japanese soldiers after the war.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Yamashita's Treasure",
			"2":"Oriental Gold",
			"3":"Eastern Treasure",
			"4":"Pearl of the Orient",
		}
	},
	12:{
		"question":"Germany belonged to which group in the WWI?",
		"description":"Germany allied with Austria-Hungary to form the Central Powers.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Central Powers",
			"2":"Allied Forces",
			"3":"Eastern Forces",
			"4":"Western Powers",
		}
	},
	13:{
		"question":"Who is the first president of USA under the U.S. Constitution?",
		"description":"George Washington became the first president of the United States ",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"George Washington",
			"2":"Abraham Lincoln",
			"3":"Donald Trump",
			"4":"John Adams",
		}
	},
	14:{
		"question":"What is the name of the treaty that sold Cuba and the Philippines to the United States?",
		"description":"The Treaty of Paris was the sign that the Spanish-American War has ended and that Spain no longer claims Cuba, Puerto Rico, Guam, and Philippines as their territory.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Treaty of Paris",
			"2":"Treaty of Versailles",
			"3":"Treaty of Peace",
			"4":"Treaty of Commonwealth",
		}
	},
	15:{
		"question":"What is the period in Chinese history which paved the way for its unification?",
		"description":"The Warring States Period was won by the Qin Dynasty, which led to the unification of the 7 warring kingdoms.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Warring States Period",
			"2":"Golden Age",
			"3":"Spring and Autumn Period",
			"4":"Ten States",
		}
	},
	16:{
		"question":"Emperor Akihito's reign is under which era?",
		"description":"Emperor Akihito ruled under the Heisei period which literally means \"peace everywhere\".",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Heisei period",
			"2":"Edo period",
			"3":"Yayoi period",
			"4":"Asuka period",
		}
	},
	17:{
		"question":"The era in Japanese history when feudal lands were replaced by prefectures is called...",
		"description":"The Meiji period, also known as Meiji restoration period, was known for its adoption of a cabinet system over feudalism.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Meiji period",
			"2":"Edo period",
			"3":"Nara period",
			"4":"Tokugawa period",
		}
	},
	18:{
		"question":"Which is the most recent country to leave the European Union?",
		"description":"United Kingdom officially left the EU on February 1, 2020 after its referendum for Brexit.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"United Kingdom",
			"2":"Greece",
			"3":"Spain",
			"4":"France",
		}
	},
	19:{
		"question":"Cleopatra was first married to...",
		"description":"Cleopatra had to marry her younger brother, Ptolemy XIII, as per Egyptian tradition.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Ptolemy XIII",
			"2":"Octavian",
			"3":"Alexander the Great",
			"4":"Julius Caesar",
		}
	},
	20:{
		"question":"When was Queen Elizabeth II crowned?",
		"description":"Queen Elizabeth was crowned on the 2nd of June 1953 in the Westminster Abbey.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"June 2, 1953",
			"2":"July 12, 1943",
			"3":"June 13, 1952",
			"4":"July 1, 1951",
		}
	},
	
	//MEDIUM
	21:{
		"question":"The International Labour Organization (ILO) was created immediately after...",
		"description":"The ILO was created after World War I, to promote peace through social justice, particularly through the labor sector.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"World War I",
			"2":"World War II",
			"3":"American Civil War",
			"4":"Cold War",
		},
	},
	22:{
		"question":"The Chinese trade route was once known as the...",
		"description":"The Silk Road refers to the network of land routes used to trade from China to South Asia, Southeast Asia, and Arabian Peninsula.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Silk Road",
			"2":"Chinese Road",
			"3":"Golden Path",
			"4":"Mainland Avenue",
		}
	},
	23:{
		"question":"What is the name of the first true pharoah of Egypt?",
		"description":"Narmer is considered as the first pharaoh to rule a unified Egypt.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Narmer",
			"2":"Hatshepsut",
			"3":"Khufu",
			"4":"Amenhotep III",
		}
	},
	24:{
		"question":"What is the shortest war?",
		"description":"The Anglo-Zanzibar War only took around 40 minutes before it ended.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Anglo-Zanzibar War",
			"2":"Cold War",
			"3":"Gulf War",
			"4":"Six Day War",
		}
	},
	25:{
		"question":"Who was the last shogun?",
		"description":"Tokugawa Yoshinobu was the last shogun, and he resigned in 1867.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Tokugawa Yoshinobu",
			"2":"Minamoto no Yoritomo",
			"3":"Minamoto no Sanetomo",
			"4":"Tokugawa Yoshimune",
		}
	},
	26:{
		"question":"During the 9/11 attack, how many planes were hijacked?",
		"description":"A total of 4 planes were hijacked but only 3 were successful in their mission, namely: two planes hitting the \"twin towers\" and a third plane hitting the Pentagon.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"4",
			"2":"3",
			"3":"2",
			"4":"1",
		}
	},
	27:{
		"question":"The so-called Great Firewall of China officially started when?",
		"description":"A regulation was passed on January 1996 to regulate the internet connections in China, which led to some domains being blocked indefinitely.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"January 1996",
			"2":"February 1994",
			"3":"December 1992",
			"4":"May 1998",
		}
	},
	28:{
		"question":"The father of Queen Elizabeth II was...",
		"description":"King George VI, together with his wife Elizabeth Angela Marguerite Bowes-Lyon, had two children, namely: Queen Elizabeth II and Princess Margaret, Countess of Snowdon.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"King George VI",
			"2":"King Philip II",
			"3":"King Edward VIII",
			"4":"King George V",
		}
	},
	29:{
		"question":"When did the Third Cholera Pandemic happen?",
		"description":"The Third Cholera Pandemic happened at around 1846 up to 1860, and it has spread beyond the borders of India.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"1860's",
			"2":"1960's",
			"3":"1820's",
			"4":"1940's",
		}
	},
	30:{
		"question":"Who is the founding father of the People's Republic of China?",
		"description":"Mao Zedong led the communist revolution in China and ruled the People's Republic of China as a chairman of the Communist Party in 1949.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Mao Zedong",
			"2":"Confucius",
			"3":"Chiang Kai-Shek",
			"4":"Emperor Taizong",
		}
	},
	31:{
		"question":"Who authored the Art of War?",
		"description":"Sun Tzu was a military strategist that authored a book about warfare called the Art of War",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Sun Tzu",
			"2":"Cao Cao",
			"3":"Mao Zedong",
			"4":"Confucius",
		},
	},
	32:{
		"question":"Who established Thanksgiving Day?",
		"description":"John Hanson used his powers as President of the Continental Congress to establish Thanksgiving Day in the year 1782.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"John Hanson",
			"2":"Abraham Lincoln",
			"3":"Benjamin Franklin",
			"4":"Franklin Roosevelt",
		}
	},
	33:{
		"question":"Which country was the first to leave the European Union?",
		"description":"Algeria left the European Union in 1962 after it had gained independence from France.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Algeria",
			"2":"United Kingdom",
			"3":"Greece",
			"4":"Germany",
		}
	},
	34:{
		"question":"Almost all of former colonies of the British Empire were under what political group?",
		"description":"Most of the newer Commonwealth of Nations members were formerly colonized by the United Kingdom.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Commonwealth of Nations",
			"2":"Britania",
			"3":"Axis Powers",
			"4":"Central Powers",
		}
	},
	35:{
		"question":"In 2019, who acted as Head of Commonwealth?",
		"description":"Queen Elizabeth II has been the Head of Commonwealth on 1952 during her accession.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Queen Elizabeth II",
			"2":"Justin Trudeau",
			"3":"Prince William",
			"4":"Prince Harry",
		}
	},
	36:{
		"question":"Who is the 125th Emperor of Japan?",
		"description":"Emperor Akihito was the 125th Emperor of Japan, and he will be posthumously known as Emperor Heisei.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Akihito",
			"2":"Naruhito",
			"3":"Suzaku",
			"4":"Daigo",
		}
	},
	37:{
		"question":"Who had the shortest reign as a king?",
		"description":"King Louis XIX, also known as Louis-Antoine of France, became king for less than a day before he decided to abdicate the throne in favor of Henry V.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Louis XIX of France",
			"2":"King Xiaowen of Qin",
			"3":"Henry II of France",
			"4":"Crateuas",
		}
	},
	38:{
		"question":"Who is the first impeached president of USA?",
		"description":"Andrew Johnson was impeached in March 1868 due to high crimes and misdemeanors. ",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Andrew Johnson",
			"2":"Bill Clinton",
			"3":"Donald Trump",
			"4":"Barack Obama",
		}
	},
	39:{
		"question":"How tall was Napoleon Bonaparte?",
		"description":"Despite common belief, Napoleon Bonaparte was actually 5'7\" tall and was around the average height of his peers.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"5'7\"",
			"2":"5'2\"",
			"3":"5'3\"",
			"4":"5'4\"",
		}
	},
	40:{
		"question":"What mainly caused the 2008 Great Recession in the United States?",
		"description":"Subprime mortgage crisis, caused by unpayable loans from people with poor credit rating, became the reason for massive economic problems for the United States.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Subprime mortgage crisis",
			"2":"Low production",
			"3":"Massive imports",
			"4":"Low government spending",
		}
	},
	
	//HARD
	41:{
		"question":"What were the British initially trading for Chinese tea?",
		"description":"The British were trading gold for tea due to their obsession, but after some time they decided to sell opium which started the \"Opium War\".",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Gold",
			"2":"Tulips",
			"3":"Salt",
			"4":"Cinnamon",
		},
	},
	42:{
		"question":"The United Nations was founded in what year?",
		"description":"The United Nations was founded in 1945 in order to foster security and peace after the Second World War.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"1945",
			"2":"1963",
			"3":"1932",
			"4":"1910",
		}
	},
	43:{
		"question":"Which event started the First World War?",
		"description":"The First World War started because of the assassination of Archduke Franz Ferdinand of Austria which sparked alliances to form.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Murder of an Archduke",
			"2":"Bombing of Pearl Harbor",
			"3":"Battle of Vienna",
			"4":"Gallipoli Campaign",
		}
	},
	44:{
		"question":"What is the title of Martin Luther King Jr.'s famous speech?",
		"description":"Marthin Luther King Jr.'s famous speech is called \"I Have a Dream\" which aims to end racism and provide equal rights for people of color in the United States.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"I Have a Dream",
			"2":"I Believe in Angels",
			"3":"What a Wonderful World",
			"4":"Believe",
		}
	},
	45:{
		"question":"What is the name of the latest Japanese era?",
		"description":"The Reiwa period was announced in April 2019 and will start after Emperor Akihito abdicates the throne.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Reiwa",
			"2":"Heisei",
			"3":"Meiji",
			"4":"Edo",
		}
	},
	46:{
		"question":"What is the other name for the Korean Demilirarized Zone?",
		"description":"The Korean Demilitarized Zone is found in the 38th parallel which was agreed upon after the occupation of the Soviet Union in the North and United States in the South.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"38th parallel",
			"2":"55th line",
			"3":"63rd parallel",
			"4":"21st amendment",
		}
	},
	47:{
		"question":"What is the name of the last king of Rome?",
		"description":"Lucius Tarquinius Superbus was the seventh and last king of Rome. No other king followed after the Romans had overthrown the Roman monarchy.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Lucius Tarquinius Superbus",
			"2":"Julius Caesar",
			"3":"Nero Augustus Germanicus",
			"4":"Lucius Aurelius Commodus",
		}
	},
	48:{
		"question":"How did Hitler die?",
		"description":"Recent studies have lead to the conclusion that Hitler died together with his wife by taking in cyanide capsules.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Suicide",
			"2":"Gunshot",
			"3":"Disease",
			"4":"Old age",
		}
	},
	49:{
		"question":"What event caused the Second World War to end?",
		"description":"The bombing of Hiroshima and Nagasaki using nuclear weapons on August 6 and 9 caused the surrender of Japan and the end of the Second World War. ",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Hiroshima Bombing",
			"2":"Bombing of Pearl Harbor",
			"3":"Death of Hitler",
			"4":"Battle of Britain",
		}
	},
	50:{
		"question":"When did the American civil war end?",
		"description":"The American civil war ended after the last battle in Texas when the last major Confederate army surrendered.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"1865",
			"2":"1860",
			"3":"1861",
			"4":"1863",
		}
	},
	51:{
		"question":"What is the name of the American general known for his line \"I shall return.\"?",
		"description":"Douglas MacArthur's famous line refers to his return to the Philippines after the Japanese invasion in World War II.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Douglas MacArthur",
			"2":"Robert Lee",
			"3":"Winfield Scott",
			"4":"George Washington",
		},
	},
	52:{
		"question":"Cleopatra had the title of...",
		"description":"Cleopatra was called as \"Queen of the Nile\" due to her prowess in establishing relations with other countries.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Queen of the Nile",
			"2":"Queen of all Egypt",
			"3":"Queen of Rome",
			"4":"The First Queen",
		}
	},
	53:{
		"question":"What important document was signed after World War I?",
		"description":"The Treaty of Versailles was signed to signify the end of the war between Germany and the Allied Powers.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Treaty of Versailles",
			"2":"Treaty of Paris",
			"3":"Congress of Vienna",
			"4":"Treaty of Tordesillas",
		}
	},
	54:{
		"question":"How much was paid by United States to Spain in the Treaty of Paris?",
		"description":"An estimated amount of $20 million was paid by the United States to Spain as compensation for developments in Philippines, Guam, and Puerto Rico.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"$20 million",
			"2":"$100 million",
			"3":"$50 million",
			"4":"$10 million",
		}
	},
	55:{
		"question":"Who was the US president with the shortest term?",
		"description":"William Harrison on had about a month during his term before he eventually died of illness. He was also the first president to die while still serving his term.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"William Harrison",
			"2":"Gerald Ford",
			"3":"Millard Fillmore",
			"4":"Martin Van Buren",
		}
	},
	56:{
		"question":"In 2019, how many British monarchs have there been since the merger of England and Scotland?",
		"description":"A total of 12 British monarchs have ruled starting with Anne of the House of Stuart.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"12",
			"2":"10",
			"3":"11",
			"4":"8",
		}
	},
	57:{
		"question":"Queen Elizabeth I was also known as...",
		"description":"Queen Elizabeth I was renowned for her protection of power by avoiding marriage; hence, she was referred to as the \"Virgin Queen\" who ruled England by herself.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Virgin Queen",
			"2":"Mightiest Queen",
			"3":"Powerful Queen",
			"4":"Deadliest Queen",
		}
	},
	58:{
		"question":"What was the worst pandemic in history?",
		"description":"The Black Death in the 1300's has caused up to 200 million casualties in Eurasia.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Black Death",
			"2":"Spanish Flu",
			"3":"SARS",
			"4":"Bird Flu",
		}
	},
	59:{
		"question":"Who wrote the Communist Manifesto?",
		"description":"Karl Marx and Friedrich Engels wrote the Communist Manifesto to explain certain theories surrounding socialism and its importance in countering capitalism.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Karl Marx",
			"2":"Mao Zedong",
			"3":"Moses Hess",
			"4":"Friedrich Nietzsche",
		}
	},
	60:{
		"question":"What was the name of the treaty that divided lands between Portugal and Spain?",
		"description":"The Treaty of Tordesillas in 1494 allowed for a peaceful division of explored lands of Portugal and Spain.",
		"answer":{
			"1":"media/graphics/answers/answer_btn.png",
			"2":"media/graphics/answers/answer_btn.png",
			"3":"media/graphics/answers/answer_btn.png",
			"4":"media/graphics/answers/answer_btn.png",
		},
		"textAnswer":{
			"1":"Treaty of Tordesillas",
			"2":"Treaty of Paris",
			"3":"Peace of Westphalia",
			"4":"Congress of Vienna",
		}
	},
};