ig.module('game.entities.correct-wrong')
.requires(
    'impact.entity',
	'plugins.textwrapper'
)
.defines(function() {
    EntityCorrectWrong = ig.Entity.extend({
    	mother:null,
    	isCorrect:false,
    	imgWrong:new ig.Image('media/graphics/sprites/wrong_panel.png'),
		imgCorrect:new ig.Image('media/graphics/sprites/correct_panel.png'),
		scale:1,
		count:180,
		textImg:null,
		isShow:false,
		finishTween:false,
		maxWidthQuestion:900,
		maxLine:4,
		fontHeight:40*2,
		sfxCorrect:"correctSound",
		sfxWrong:"wrongSound",
		
    	init:function(x,y,settings)
    	{
    		this.parent(x,y,settings);
			this.btnNext = ig.game.spawnEntity(EntityButtonImage,-1000,this.pos.y+130,{
							pathImage:"media/graphics/sprites/next_btn.png",
							clickCallBack:this.onNext,
							clickCallBackContext:this,
							buttonScale:1,
						});
    		// this.showAnswer();

    	},
		onNext:function(){
			this.mother.changeQuestion();
			this.isShow=false;
			this.finishTween=false;
			this.btnNext.pos.x=-1000;
		},
    	showAnswer:function(){
			
    		this.scale=0.1;
			if(this.isCorrect){
				ig.soundHandler.sfxPlayer.play(this.sfxCorrect);
			}else{
				ig.soundHandler.sfxPlayer.play(this.sfxWrong);
			} 
    		this.tween({
	           	scale:1
	        },
	        0.3
	        ,{
	            onComplete:function(){
					this.finishTween=true;
					this.btnNext.pos.x=ig.system.width/3+50;
				}.bind(this),
	             easing:ig.Tween.Easing.Bounce.EaseOut
	        }).start(); 
    	},
    	delayHide:function()
    	{
    		this.scale=1;
    		this.tween({
	           	scale:0.1
	        }
	        ,0.1
	        ,{
	        	delay:0.6,
	            onComplete:function()
	                {
    					
    					
	                }.bind(this),
	             easing:ig.Tween.Easing.Linear.EaseNone
	        }).start(); 
    	},
    	update:function(){
    		this.parent();
    		// if(this.isShow&&this.dela)
    		// {
	    	// 	this.count--;
	    	// 	if(this.count==0)
	    	// 	{
	    	// 		if( this.awal)this.awal.stop();
	    			
	    	// 		 this.awal.start();
	    	// 		console.log("jfae ");
	    	// 		// this.tween({scale:0.1},0.3,{
	    	// 		// 	onComplete:function(){
	    	// 		// 		this.count=180;
	    	// 		// 		this.mother.changeQuestion();
	    	// 		// 		this.isShow=false;
	    	// 		// 	}.bind(this)
	    	// 		// }).start();
	    	// 	}
	    	// }
    	},
    	draw:function()
    	{
    		this.parent();
    		if(this.isShow)
    		{
	    		var ctx=ig.system.context;
	    		var img;
				var textCorrect;
				
	    		if(this.isCorrect){
					textCorrect="Correct";
					img = this.imgCorrect;
				}else{
					textCorrect="Oops!";
					img = this.imgWrong;
				} 

	    		var x=this.pos.x-img.width*this.scale/2;
	    		var y=this.pos.y-img.height*this.scale/2;
	    		var w=img.width*this.scale;
	    		var h=img.height*this.scale;

	    		ctx.save();
	    		ctx.drawImage(img.data,0,0,img.width,img.height,
	    			x,y,
	    			w,h
	    		);
				
				
				//console.log(this.textImg)
				
				if(this.textImg!=null){
					ctx.save();
					ctx.fillStyle = "#ffffff";
					ctx.font = '70px quicksand-bold';
					ctx.textAlign = "center";
					ctx.textBaseline = "alphabetic";
					ctx.fillText(textCorrect,ig.system.width/2,y+115);
					//ctx.fillText(this.textImg,this.pos.x+150,this.pos.y+50);
					ctx.restore();
				}
				
				if(this.finishTween==true){
					var textwrapper = new ig.Textwrapper(42,"quicksand-regular");
					textwrapper.textLineHeight=57;
					textwrapper.textBaseline="alphabetic";
					textwrapper.textAlign="center";
					textwrapper.textSpacing=1;
					var textlist = textwrapper.wrapText(this.textImg, this.maxWidthQuestion);
					ctx.save();
					ctx.fillStyle = "#ffffff";
					ctx.textAlign = "left";
					textwrapper.drawTextList(textlist,ig.system.width/2,this.pos.y-90);
					ctx.restore();
				}
	    		ctx.restore();
	    	}
    	}
    });
});