ig.module('game.entities.controller.game-control')
.requires(
	'impact.entity',
	'game.entities.buttons.button-image',
	'game.entities.correct-wrong',
	'game.entities.buttons.button-sound',
	'plugins.textwrapper'
)
.defines(function() {
	EntityGameControl = ig.Entity.extend({
		fontSize:40*2,
		fontHeight:40*2,
		maxWidthQuestion:2*500,
		imgBG:[new ig.Image('media/graphics/backgrounds/desktop/cover.png'),
				new ig.Image('media/graphics/backgrounds/desktop/bg01.png'),
				new ig.Image('media/graphics/backgrounds/desktop/bg02.png'),
				new ig.Image('media/graphics/backgrounds/desktop/bg03.png'),
				new ig.Image('media/graphics/backgrounds/desktop/bg04.png'),
				new ig.Image('media/graphics/backgrounds/desktop/bg05.png'),
				new ig.Image('media/graphics/backgrounds/desktop/bg06.png'),
				new ig.Image('media/graphics/backgrounds/desktop/bg07.png'),
				new ig.Image('media/graphics/backgrounds/desktop/bg08.png'),
				new ig.Image('media/graphics/backgrounds/desktop/bg09.png'),
				new ig.Image('media/graphics/backgrounds/desktop/bg10.png'),
			],
		imgQuestion:new ig.Image('media/graphics/sprites/question_panel.png'),
		numQuestion:1,
		maxLine:4,
		numAnswer:4,
		arrAnswer:[],
		
		
		isCorrect:false,
		timeChange:180,
		countChange:0,
		bolAnswer:false,
		enabled:true,
		backgroundImg:0,
		totalBg:11,
		questionsCount:0,

		randomEasy:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
		randomMedium:[21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40],
		randomHard:[41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60],
		randomQuestions:null,
		// img1:new ig.Image("media/graphics/answers/q1a1.png"),
		// img2:new ig.Image("media/graphics/answers/q1a2.png"),
		// img3:new ig.Image("media/graphics/answers/q1a3.png"),
		// img4:new ig.Image("media/graphics/answers/q1a4.png"),

		init:function(x,y,settings)
		{
			this.parent(x,y,settings);
			/* var key, count = 0;
			for(key in _Question) {
			  if(_Question.hasOwnProperty(key)) {
				this.questionsCount = count++;
			  }
			} */
			
			switch(ig.game.difficulty){
				case 'easy':
					this.numQuestion = 1;
					this.questionsCount = 21;
					this.randomQuestions = this.shuffleArray(this.randomEasy);
				break;
				
				case 'medium':
					this.numQuestion = 1;
					this.questionsCount = 21;
					this.randomQuestions = this.shuffleArray(this.randomMedium);
				break;
				
				case 'hard':
					this.numQuestion = 1;
					this.questionsCount = 21;
					this.randomQuestions = this.shuffleArray(this.randomHard);
				break;
			}
			ig.game.totalQuestion=this.questionsCount-1;
			//console.log(ig.game.totalQuestion)
			
			this.numAnswer=ig.game.answerTotal;
			this.loadAnswer();
			ig.game.numCorrect=0;
			ig.game.spawnEntity(EntityBlackScreen,0,0,{
				isFadeIn:false,
				zIndex:this.zIndex+20
			});
			this.result=ig.game.spawnEntity(EntityCorrectWrong,ig.system.width/2,ig.system.height/2,{
				isCorrect:false,
				mother:this,
				zIndex:this.zIndex+10
			});
			
			this.btnSound=ig.game.spawnEntity(EntityButtonSound,ig.system.width-100,50,{});

			this.btnHome = ig.game.spawnEntity(EntityButtonImage,ig.system.width-100,140,{
				pathImage:"media/graphics/sprites/home_btn_game.png",
				clickCallBack:this.onHome,
				clickCallBackContext:this
			});

			ig.game.sortEntitiesDeferred();
		},

		onHome:function(){
			this.enabled=false;
			ig.game.spawnEntity(EntityBlackScreen,0,0,{
				to:"LevelMainMenu"
			});
		},

		shuffleArray: function(array) {
                var counter = array.length,
                    temp, index;
                // While there are elements in the array
                while (counter > 0) {
                    // Pick a random index
                    index = Math.floor(Math.random() * counter);
                    // Decrease counter by 1
                    counter--;
                    // And swap the last element with it
                    temp = array[counter];
                    array[counter] = array[index];
                    array[index] = temp;
                }
                return array;
        },

		onRetry:function(){
			ig.game.spawnEntity(EntityBlackScreen,0,0,{
				to:"LevelGame",
				zIndex:this.zIndex+20
			});
			ig.game.sortEntitiesDeferred();
			// ig.game.director.jumpTo(LevelGame);
		},
		// onPlay:function(){
		// 	ig.game.director.jumpTo(LevelGame);
		// },
		onAnswer:function(name){
			if(this.enabled)
			{
				this.enabled=false;
				this.result.isShow=true;
				this.result.count=90;
				if(name=="a1")
				{
					if (adDetails.settings.gameAnalytics === "on") {
                        try {
                            amplitude.getInstance().logEvent("Question "+this.numQuestion+ " Correct");
                        } catch (error) {
                            console.log(error);
                        }
                    }
                    
					this.result.isCorrect=true;
					ig.game.numCorrect++;
				}
				else
				{
					if (adDetails.settings.gameAnalytics === "on") {
                        try {
                            amplitude.getInstance().logEvent("Question "+this.numQuestion+ " Wrong");
                        } catch (error) {
                            console.log(error);
                        }
                    }
				 	this.result.isCorrect=false;
				}
				this.result.showAnswer();
				
			}
		},
		changeQuestion:function(){
			this.enabled=true;
			this.backgroundImg = Math.floor(Math.random()*7);
			if(this.numQuestion<ig.game.totalQuestion)
			{				
				this.numQuestion++;
				
				this.loadAnswer();
			}
			else
				this.doneStage();
		},
		doneStage:function(){
			if(this.enabled)
			{
				if (adDetails.settings.gameAnalytics === "on") {
                    try {
                        amplitude.getInstance().logEvent('Game Ended');
                    } catch (error) {
                        console.log(error);
                    }
                }
				this.enabled=false;
				ig.game.spawnEntity(EntityBlackScreen,0,0,{
					to:"LevelResult"
				});
			}
			// ig.game.director.jumpTo(LevelResult);
		},
		loadAnswer:function(){
			if (adDetails.settings.gameAnalytics === "on") {
                try {
                    amplitude.getInstance().logEvent("Question "+this.numQuestion+ " loaded");
                } catch (error) {
                    console.log(error);
                }
            }
			
			if(ig.game.answerTotal==4){
				var arrIdx=[0,1,2,3];
			}
			else if(ig.game.answerTotal==3){
				var arrIdx=[0,1,2];
			}
			else if(ig.game.answerTotal==2){
				var arrIdx=[0,1];
			}
			
			var plusX=(ig.game.sizeAnswerBox-_Question.size.x);
			var plusY=(ig.game.sizeAnswerBox-_Question.size.y);
			var wAll=this.numAnswer*(_Question.size.x)+ig.game.multiply*20*3;
			var posX=(ig.system.width/4 - 20);
			// ig.system.width/this.numAnswer*(_Question.size.x)
			for(var i=0;i<this.numAnswer;i++)
			{
				if(ig.game.answerTotal==4){
					if(i==0){
						posX=(ig.system.width/4 - 20);
						plusY=(ig.system.height/2+20);
					}
					else if(i==1){
						posX=(ig.system.width/4 - 20);
						plusY=(ig.system.height/2+170);
						
					}
					else if(i==2){
						posX=(ig.system.width/2 + 40);
						plusY=(ig.system.height/2+20);
					}
					else if(i==3){
						posX=(ig.system.width/2 + 40);
						plusY=(ig.system.height/2+170);
					}
				}
				else if(ig.game.answerTotal==3){
					if(i==0){
						posX=(ig.system.width/4 - 20);
						plusY=(ig.system.height/2+20);
					}
					else if(i==1){
						posX=(ig.system.width/2 + 40);
						plusY=(ig.system.height/2+20);
						
					}
					else if(i==2){
						posX=(ig.system.width/2-150);
						plusY=(ig.system.height/2+170);
					}
				}
				else if(ig.game.answerTotal==2){
					if(i==0){
						posX=(ig.system.width/4 - 20);
						plusY=(ig.system.height/2+20);
					}
					else if(i==1){
						posX=(ig.system.width/2 + 40);
						plusY=(ig.system.height/2+20);
						
					}
				}
				
				if(this.arrAnswer.length<this.numAnswer)
				{
					var rand=Math.floor(Math.random() *(arrIdx.length) );

					var posXAnswer=posX+arrIdx[rand]*(_Question.size.x+20*ig.game.multiply);
					var button=ig.game.spawnEntity(EntityButtonImage,posX+arrIdx[rand],plusY,{
						pathImage:_Question[this.randomQuestions[this.numQuestion-1]].answer[i+1],
						textImg:_Question[this.randomQuestions[this.numQuestion-1]].textAnswer[i+1],
						clickCallBackContext:this,
						clickCallBack:this.onAnswer,
						name:"a"+(i+1).toString(),
						zIndex:this.zIndex+i+1,
						mother:this,
						fontSize:32,
						place:'answerBtn',
						// pathImage:"media/graphics/answers/q1a"+(i+1)+".png"
					});
					arrIdx.splice(rand,1);
					this.arrAnswer.push(button);
				}
				else
				{
					var button=this.arrAnswer[i];
					var rand=Math.floor(Math.random()*(arrIdx.length) );
					button.changeImage(_Question[this.randomQuestions[this.numQuestion-1]].answer[arrIdx[rand]+1]);
					button.textImg = _Question[this.randomQuestions[this.numQuestion-1]].textAnswer[arrIdx[rand]+1];
					button.name="a"+(arrIdx[rand]+1).toString();
					arrIdx.splice(rand,1);
				}
				
			}
		},
		draw:function()
		{
			// this.imgBG.draw(0,0);

			
			var ctx=ig.system.context;
			ctx.save();
			ctx.drawImage(this.imgBG[this.backgroundImg].data,0,0,this.imgBG[this.backgroundImg].width,this.imgBG[this.backgroundImg].height,
			0,0,ig.system.width,ig.system.height);
			ctx.restore();
			
			this.imgQuestion.draw(110,50);
			
			ctx.save();
			ctx.fillStyle = "#ffffff";
			ctx.font = ig.game.fontSize + "px quicksand-regular";
			ctx.textAlign="left";
			var wQ=ctx.measureText(_STRINGS.Game.question+this.numQuestion).width;
			//this.fillTextWithSpacing(ctx,_STRINGS.Game.question+this.numQuestion,ig.system.width/2-wQ/2, 65*2,1);
			// ctx.fillText(_STRINGS.Game.question+this.numQuestion, ig.system.width/2, 65+30);	
			

			var textwrapper = new ig.Textwrapper(60, "quicksand-regular");
			textwrapper.textLineHeight=ig.game.fontHeight;
			textwrapper.textSpacing=1;
			textwrapper.textAlign="center";
			var textlist = textwrapper.wrapText(_Question[this.randomQuestions[this.numQuestion-1]].question, this.maxWidthQuestion);
			this.result.textImg = _Question[this.randomQuestions[this.numQuestion-1]].description;
			//console.log(_Question[this.numQuestion].description)
			ctx.save();
			ig.system.context.fillStyle = "#ffffff";
			ig.system.context.textAlign = "left";
			
			textwrapper.drawTextList(textlist, ig.system.width/2, 2*50+((this.maxLine-textlist.length)*this.fontHeight)/2-5);
			ctx.restore();
			
			//console.log(this.numQuestion)
			
		},
		
		fillTextWithSpacing:function(context, text, x, y, spacing)
	    {
	        //Start at position (X, Y).
	        //Measure wAll, the width of the entire string using measureText()
	        var wAll = context.measureText(text).width;
	        var wShorter=0;
	        do
	        {
		        //Remove the first character from the string
		        var char = text.substr(0, 1);
		        text = text.substr(1);
		        //Print the first character at position (X, Y) using fillText()
		        context.fillText(char, x, y);
		        //Measure wShorter, the width of the resulting shorter string using measureText().
		        if (text == "")
		            wShorter = 0;
		        else
		            wShorter = context.measureText(text).width;
		        //Subtract the width of the shorter string from the width of the entire string, giving the kerned width of the character, wChar = wAll - wShorter
		        var wChar = wAll - wShorter;
		        //Increment X by wChar + spacing
		        x += wChar -spacing;
		        //wAll = wShorter
		        wAll = wShorter;
		        
		        //Repeat from step 3
	        } while (text != "");
	    },
	});
});