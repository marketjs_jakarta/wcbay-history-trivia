ig.module('game.entities.controller.main-menu-control')
.requires(
	'impact.entity',
	'game.entities.buttons.button-text',
	'game.entities.black-screen'
)
.defines(function() {
	EntityMainMenuControl = ig.Entity.extend({
		imgBG:new ig.Image('media/graphics/splash/splash.png'),
		enabled:true,
		init:function(x,y,settings)
		{
			
			this.parent(x,y,settings);
			this.btnPlay = ig.game.spawnEntity(EntityButtonImage,ig.system.width/2-65,ig.system.height/2+80,{
				pathImage:"media/graphics/sprites/play_btn.png",
				clickCallBackContext:this,
				clickCallBack:this.onPlay,
				name:"btn-play",
				zIndex:this.zIndex+1,
				mother:this
			});
			
			
			
			//buttons difficulty
			this.btnEasy = ig.game.spawnEntity(EntityButtonImage,-1000,ig.system.height/2+40,{
				textImg:"Easy",
				pathImage:"media/graphics/sprites/red_btn.png",
				clickCallBackContext:this,
				clickCallBack:this.onStart,
				name:"btn-difficulty",
				zIndex:this.zIndex+1,
				mother:this,
				place:'easybtn',
			});
			
			this.btnMedium = ig.game.spawnEntity(EntityButtonImage,-1000,ig.system.height/2+140,{
				textImg:"Medium",
				pathImage:"media/graphics/sprites/red_btn.png",
				clickCallBackContext:this,
				clickCallBack:this.onStart,
				name:"btn-difficulty",
				zIndex:this.zIndex+1,
				mother:this,
				place:'mediumbtn',
			});
			
			this.btnHard = ig.game.spawnEntity(EntityButtonImage,-1000,ig.system.height/2+240,{
				textImg:"Hard",
				pathImage:"media/graphics/sprites/red_btn.png",
				clickCallBackContext:this,
				clickCallBack:this.onStart,
				name:"btn-difficulty",
				zIndex:this.zIndex+1,
				mother:this,
				place:'hardbtn',
			});
			
			/*ig.game.spawnEntity(ig.FullscreenButton, 5, 5, { 
				enterImage: new ig.Image("media/graphics/sprites/enter-fullscreen.png"), 
				exitImage: new ig.Image("media/graphics/sprites/exit-fullscreen.png") 
			});
			ig.game.spawnEntity(EntityButtonMoreGames, 5, 100)*/
		},
		onPlay:function(){
			this.btnPlay.pos.x = -1000;
			this.btnEasy.pos.x = ig.system.width/2-170;
			this.btnMedium.pos.x = ig.system.width/2-170;
			this.btnHard.pos.x = ig.system.width/2-170;
		},
		
		onStart:function(){
			if(this.enabled)
			{
				if (adDetails.settings.gameAnalytics === "on") {
                    try {
                        amplitude.getInstance().logEvent("Play Button Clicked");
                    } catch (error) {
                        console.log(error);
                    }
                }
				this.enabled=false;
				
				
				ig.game.spawnEntity(EntityBlackScreen,0,0,{
					to:"LevelGame"
				});
			}
			// ig.game.director.jumpTo(LevelGame);
		},
		draw:function()
		{
			var ctx=ig.system.context;
			ctx.save();
			ctx.drawImage(this.imgBG.data,0,0,this.imgBG.width,this.imgBG.height,
			0,0,ig.system.width,ig.system.height);
			ctx.restore();
			// this.imgBG.draw(0,0);
		}
	});
});