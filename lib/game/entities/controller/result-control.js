ig.module('game.entities.controller.result-control')
.requires(
	'impact.entity'
)
.defines(function() {
	EntityResultControl = ig.Entity.extend({
		imgBG:new ig.Image("media/graphics/sprites/bg-result.png"),
		enabled:true,
		init:function(x,y,settings)
		{
			this.parent(x,y,settings);
			ig.game.spawnEntity(EntityBlackScreen,0,0,{
				isFadeIn:false
			});
			ig.game.spawnEntity(EntityButtonImage,(ig.system.width-ig.game.multiply*100)/2+100,ig.system.height/2+155,{
				pathImage:"media/graphics/sprites/replay_btn.png",
				clickCallBack:this.onRetry,
				clickCallBackContext:this
			});
			
			ig.game.spawnEntity(EntityButtonImage,(ig.system.width-ig.game.multiply*100)/2-50,ig.system.height/2+155,{
				pathImage:"media/graphics/sprites/home_btn.png",
				clickCallBack:this.onHome,
				clickCallBackContext:this
			});
			if (adDetails.settings.gameAnalytics === "on") {
                try {
                    amplitude.getInstance().logEvent("Correct: "+ig.game.numCorrect);
                    amplitude.getInstance().logEvent("Wrong: "+(ig.game.totalallQuestion-ig.game.numCorrect));
                } catch (error) {
                    console.log(error);
                }
            }
		},
		onRetry:function(){
			if(this.enabled)
			{
				if (adDetails.settings.gameAnalytics === "on") {
                    try {
                        amplitude.getInstance().logEvent("Replay Button Clicked");
                    } catch (error) {
                        console.log(error);
                    }
                }
				this.enabled=false;
				ig.game.spawnEntity(EntityBlackScreen,0,0,{
					to:"LevelGame"
				});
			}
		},
		onHome:function(){
			if(this.enabled)
			{
				this.enabled=false;
				ig.game.spawnEntity(EntityBlackScreen,0,0,{
					to:"LevelMainMenu"
				});
			}
		},
		draw:function()
		{
			var ctx=ig.system.context;
			ctx.save();
			ctx.drawImage(this.imgBG.data,0,0,this.imgBG.width,this.imgBG.height,
			0,0,ig.system.width,ig.system.height);
			ctx.restore();
			
			//bravo
			var textwrapper2 = new ig.Textwrapper(ig.game.multiply*78, "quicksand-bold");
			textwrapper2.textLineHeight=ig.game.multiply*78;
			textwrapper2.textSpacing=ig.game.multiply*4;
			textwrapper2.textAlign="center";

			ctx.save();
			ctx.fillStyle ="#ffffff";
			ctx.font = "50px quicksand-regular";
			ctx.textAlign="left";
			var strBravo="";
			if(ig.game.numCorrect>=_Question.marginCorrectAnswer)
			{
				strBravo=_STRINGS.Results.bravo1;
				var textlist3 = textwrapper2.wrapText(strBravo, ig.game.multiply*960);
				textwrapper2.drawTextList(textlist3, ig.system.width/2-ig.game.multiply*4-12, ig.system.height/4);
			}
			else
			{
				strBravo=_STRINGS.Results.bravo2;
				var textlist3 = textwrapper2.wrapText(strBravo, ig.game.multiply*960);
				textwrapper2.drawTextList(textlist3, ig.system.width/2-ig.game.multiply*4-2, ig.system.height/4);
			}
			
			// this.fillTextWithSpacing(ctx,strBravo,ig.system.width/2+strBravo.length, ig.game.multiply*180,ig.game.multiply*4);
			// ctx.fillText(_STRINGS.Results.bravo, ig.system.width/2, 180);	
			ctx.restore();

			//congrats
			// ctx.save();
			// ctx.fillStyle ="#0568af";
			// ctx.font = 44 + "px quicksand-bold";
			// ctx.textAlign="left";
			// this.fillTextWithSpacing(ctx,_STRINGS.Results.result1,ig.system.width/2, 180+70,1);
			// // ctx.fillText(_STRINGS.Results.result1, ig.system.width/2, 180+70);	
			// ctx.restore();

			var textwrapper = new ig.Textwrapper(ig.game.multiply*35, "quicksand-bold");
			textwrapper.textLineHeight=ig.game.multiply*35;
			textwrapper.textSpacing=1;
			textwrapper.textAlign="center";
			var textlist = textwrapper.wrapText(_STRINGS.Results.result1, ig.game.multiply*960);
			
			ctx.save();
			ig.system.context.fillStyle = "#ffffff";
			ig.system.context.textAlign = "left";
			
			textwrapper.drawTextList(textlist, ig.system.width/2+13,ig.system.height/2);
			ctx.restore();

			var str=ig.game.numCorrect+_STRINGS.Results.result2+ig.game.totalallQuestion+_STRINGS.Results.result3;
			var textlist2 = textwrapper.wrapText(str, 960);
			
			ctx.save();
			ig.system.context.fillStyle = "#ffffff";
			ig.system.context.textAlign = "left";
			
			textwrapper.drawTextList(textlist2, ig.system.width/2,ig.system.height/2+100);
			ctx.restore();
		},
		// fillTextWithSpacing:function(context, text, x, y, spacing)
	 //    {
	 //        //Start at position (X, Y).
	 //        //Measure wAll, the width of the entire string using measureText()

	 //        var wAll = context.measureText(text).width;
	 //        x-=wAll/2;
	 //        var wShorter=0;
	 //        do
	 //        {
		//         //Remove the first character from the string
		//         var char = text.substr(0, 1);
		//         text = text.substr(1);
		//         //Print the first character at position (X, Y) using fillText()
		//         context.fillText(char, x, y);
		//         //Measure wShorter, the width of the resulting shorter string using measureText().
		//         if (text == "")
		//             wShorter = 0;
		//         else
		//             wShorter = context.measureText(text).width;
		//         //Subtract the width of the shorter string from the width of the entire string, giving the kerned width of the character, wChar = wAll - wShorter
		//         var wChar = wAll - wShorter;
		//         //Increment X by wChar + spacing
		//         x += wChar -spacing;
		//         //wAll = wShorter
		//         wAll = wShorter;
		        
		//         //Repeat from step 3
	 //        } while (text != "");
	 //    },
	});
});