ig.module('game.entities.buttons.button-image')
.requires(
	'game.entities.buttons.button',
	'plugins.data.vector',
	'plugins.textwrapper'
)
.defines(function() {
	EntityButtonImage = EntityButton.extend({
		collides:ig.Entity.COLLIDES.NEVER,
		type:ig.Entity.TYPE.A,
		size:new Vector2(39,44),
		fillColor:null,
		zIndex:76,
		img:null,
		// img:new ig.Image('media/graphics/sprites/button/btn-gallery.png'),
		isClicked:false,
		timerPause:10,
		clickCallBack:null,
		clickCallBackContext:null,
		mother:null,
		name:"button-image",
		textImg:null,
		fontSize:36,
		buttonScale:1,
		imgScale:1,
		offSet:{x:0,y:0},
		bolUpdateSize:false,
		isShow:true,
		enabled:true,
		place:null,
		sfxButton:"buttonSound",
		
		init:function(x,y,settings)
		{
			this.parent(x,y,settings);
			this.img=new ig.Image(settings.pathImage);	
			this.size=new Vector2(this.img.width*this.buttonScale*this.imgScale,this.img.height*this.buttonScale*this.imgScale);
		
		},

		draw:function()
		{
			if(this.isShow)
			{
				this.parent();
				
				var ctx=ig.system.context;
				ctx.save();

				ctx.drawImage(this.img.data,
					0,0,
					this.img.width,this.img.height,
					this.pos.x+(this.img.width*this.imgScale*(1-this.buttonScale)/2),
					this.pos.y+(this.img.height*this.imgScale*(1-this.buttonScale)/2),
					this.img.width*this.buttonScale*this.imgScale,
					this.img.height*this.buttonScale*this.imgScale
				);	
				
				if(this.textImg!=null){
					var xx,yy;
					var str = this.textImg;
					var maxWidth = 310;

					var textwrapper = new ig.Textwrapper(this.fontSize, "quicksand-bold");
					var textlist = textwrapper.wrapText(str, maxWidth);
					if(this.place=='answerBtn'){
						if(textlist.length==1){
							textwrapper.textFontSize=this.fontSize*this.buttonScale*this.imgScale;
							xx = 155;
							yy = 55;
						}else{
							textwrapper.textFontSize=this.fontSize*this.buttonScale*this.imgScale-10;
							xx = 165;
							yy = 37;
						}
					}else{
						textwrapper.textFontSize=this.fontSize*this.buttonScale*this.imgScale;
						xx = 155;
						yy = 60;
					}
					
					textwrapper.textAlign="center";
					textwrapper.textBaseline="alphabetic";
					textwrapper.textLineHeight=30;
					textwrapper.textSpacing=1;
					var textlistHeight=textlist.length*ig.system.context.measureText("W").width;
					ig.system.context.fillStyle = "#ffffff";
					ig.system.context.textAlign = "left";
					textwrapper.drawTextList(textlist,this.pos.x+xx,this.pos.y+yy);
					
					/* ctx.fillStyle = "#ffffff";
					ctx.font = this.fontSize+'px quicksand-bold';
					ctx.textAlign = "center";
					ctx.textBaseline = "alphabetic";
					ctx.fillText(this.textImg,this.pos.x+165,this.pos.y+60); */
				}			
				ctx.restore();
			}
		},

		update:function()
		{
			this.parent();
			
			if(this.bolUpdateSize==false&&this.img.loaded)
			{
				this.size=new Vector2(this.img.width*this.buttonScale*this.imgScale,this.img.height*this.buttonScale*this.imgScale);
				this.bolUpdateSize=true;
			}

			if(this.isClicked)
			{
				if(this.buttonScale>0.9)
				{
					this.buttonScale-=0.1;
					//this.fontSize = this.fontSize-2; 
				}
				
				this.timerPause--;
				if(this.timerPause==0)
				{
					this.isClicked=false;
					ig.game.tweening=false;
					this.clickCallBack.apply(this.clickCallBackContext, [this.name]);  
					this.buttonScale=1;
					//this.fontSize = this.fontSize+2;
					this.timerPause=10;
				}
			}
		},
		changeImage:function(path)
		{
			this.img=new ig.Image(path);	
		},
		clicked:function(){

			if((this.mother&&this.mother.enabled)||this.mother==null&&this.isShow)
			{
				if(ig.game.tweening==false&&this.isClicked==false)
				{
					this.parent();
					ig.soundHandler.sfxPlayer.play(this.sfxButton);
					ig.game.tweening=true;
					this.isClicked=true;
					
					switch(this.place){
						case 'easybtn':
							ig.game.difficulty = 'easy';
						break;
						
						case 'mediumbtn':
							ig.game.difficulty = 'medium';
						break;
						
						case 'hardbtn':
							ig.game.difficulty = 'hard';
						break;
					}
					
				}
			}
		},

		clicking:function(){
			
		},
		released:function(){
			
		},
	});
});