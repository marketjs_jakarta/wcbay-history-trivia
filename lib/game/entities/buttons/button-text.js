ig.module('game.entities.buttons.button-text')
.requires(
	'game.entities.buttons.button'
)
.defines(function() {
	EntityButtonText = EntityButton.extend({
		text:"BUTTON",
		fontSize:55,
		fontColor:"#fff",
		buttonColor:"#0077cc",
		size:new Vector2(243,73),
		rounded:15,
		clickCallBack:null,
		clickCallBackContext:null,
		enabled:true,
		clickPos:0,
		timeCount:0,
		init:function(x,y,settings)
		{
			this.parent(x,y,settings);
			this.fontSize*=ig.game.multiply;
			this.rounded*=ig.game.multiply;
			this.size.x*=ig.game.multiply;
			this.size.y*=ig.game.multiply;
		},
		clicked:function(){
			if(this.enabled)
			{
				this.enabled=false;
				this.buttonColor="#0568af";
				this.clickPos=10;
				
			}
		},
		update:function(){
			this.parent();
			if(this.enabled==false)
			{
				this.timeCount++;
				if(this.timeCount>40)
				{
					this.clickCallBack.apply(this.clickCallBackContext, [this.name]);  
				}
			}
		},
		draw:function()
		{
			this.parent();
			var ctx=ig.system.context;

			ctx.save();
			ig.game.createRoundRect(this.pos.x,this.pos.y+ig.game.multiply*10,this.size.x,this.size.y,this.rounded);
			ctx.globalAlpha=0.4;
			ctx.fillStyle="#000";
			ctx.fill();
			ctx.restore();

			ctx.save();
			ig.game.createRoundRect(this.pos.x,this.pos.y+this.clickPos,this.size.x,this.size.y,this.rounded);
			ctx.fillStyle=this.buttonColor;
			ctx.strokeStyle="#fff";
			ctx.lineWidth=ig.game.multiply*5;
			ctx.stroke();
			ctx.fill();
			ctx.restore();

			ctx.save();
			ctx.fillStyle = this.fontColor;
			ctx.font = this.fontSize + "px quicksand-bold";
			ctx.fillText(this.text, this.pos.x + (this.size.x-ig.system.context.measureText(this.text).width)/2, this.clickPos+this.pos.y+this.size.y-ig.game.multiply*20);	
			ctx.restore();
		}
	});
});