ig.module('game.entities.buttons.button-sound')
.requires(
	'game.entities.buttons.button'
)
.defines(function() {
	EntityButtonSound = EntityButton.extend({
		type:ig.Entity.TYPE.A,
		gravityFactor:0,
		animSheet: new ig.AnimationSheet('media/graphics/sprites/sound_btn.png',80,80),
		zIndex:10001,
		size:{x:80,
			y:80,
		},
        
        mutetest:false,
        
		name:"soundtest",
		init:function(x,y,settings){
			this.parent(x,y,settings);
			if(ig.global.wm)
			{
    			
                
				return;
			}
            this.addAnim('on', 1,[0]);
			this.addAnim('off',1,[1]);
			
			//GET SOUND STATE
			if(ig.game.soundOn){
				this.currentAnim = this.anims.on;
			}else{
				this.currentAnim = this.anims.off;
			}
		},
        draw:function()
        {
            this.parent();
            //ig.system.context.fillRect(this.pos.x,this.pos.y,50,50);
        },
        clicked:function()
		{
            console.log("pressed");
			ig.game.soundOn = !ig.game.soundOn;
			if(ig.game.soundOn){
				ig.soundHandler.unmuteAll(true);
				this.currentAnim = this.anims.on;
				ig.game.storage.set('trivia-soundOn',true);
			}else{
				ig.soundHandler.muteAll(true);
				this.currentAnim = this.anims.off;
				ig.game.storage.set('trivia-soundOn',false);
			}
			
		},
		clicking:function()
		{
			
		},
		released:function()
		{
			
		}
	});
});