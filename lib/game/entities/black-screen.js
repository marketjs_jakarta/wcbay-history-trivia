ig.module('game.entities.black-screen')
.requires(
	'impact.entity'
)
.defines(function() {
	EntityBlackScreen = ig.Entity.extend({
		from:null,
		to:null,
		alpha:0,
		isFadeIn:true,
		init:function(x,y,settings)
		{
			this.parent(x,y,settings);
			if(this.isFadeIn==false)
			{
				this.alpha=1;
				this.tween({alpha:0},0.3,{easing:ig.Tween.Easing.Linear.EaseNone,onComplete:this.fadeOut.bind(this)}).start();
			}
			else this.tween({alpha:1},0.3,{easing:ig.Tween.Easing.Linear.EaseNone,onComplete:this.fadeOut.bind(this)}).start();
		},
		fadeOut:function(){
			// this.tween({alpha:0},0.3,{easing:ig.Tween.Easing.Linear.EaseNone,delay:0.5,onComplete:function(){
			if(this.isFadeIn)
			{
				if(this.to=="LevelGame"){
					ig.game.director.jumpTo(LevelGame);
				}	
				else if(this.to=="LevelResult"){
					ig.game.director.jumpTo(LevelResult);
				}
				else if(this.to=="LevelMainMenu"){
					ig.game.director.jumpTo(LevelMainMenu);
				}	
			}
			else
			{
				this.kill();
			}
			// }.bind(this)}).start();
		},
		update:function(){
			this.parent();
		},
		draw:function()
		{
			this.parent();
			var ctx=ig.system.context;
			ctx.save();
			ctx.fillStyle="#000";
			ctx.globalAlpha=this.alpha;
			ctx.fillRect(0,0,ig.system.width,ig.system.height);
			ctx.restore();
		}
	});
});