// Jason Low's Textwrapper Class
// jason.low@impact360design.com
// ver 1.2

// Notes:
// v1.1 - added error checking
// v1.2 - replaced single quotes with double quotes for strings

// Usage Examples:
/*
var str = "HelloWorld HelloWorld HelloWorld HelloWorld";
var maxWidth = 100;

var textwrapper = new ig.Textwrapper(14, "Impact, Charcoal, sans-serif");
var textlist = textwrapper.wrapText(str, maxWidth);

ig.system.context.fillStyle = "#ffffff";
ig.system.context.textAlign = "left";
textwrapper.drawTextList(textlist, 100, 100);
*/

ig.module( "plugins.textwrapper" )
.requires(
)
.defines(function(){

ig.Textwrapper = ig.Class.extend({
    textFontSize: 12,
    textFont: "quicksand-bold, Charcoal, wwbd",
    textLineHeight: 12,
    textHeight:0,
    textSpacing:0,
    textAlign:"left",
	wordLength:1,
    init: function(size, font){
        if(size){
            this.textFontSize = size;
            this.textLineHeight = size;
        }
        if(font){
            this.textFont = font;
        }
    },

    wrapText: function(text, maxWidth) {
        if(!text || text == "") return [];
        if(maxWidth <= 0) return [];

        var ctx = ig.system.context;
        ctx.font = this.textFontSize + "px " + this.textFont;

        var words = text.split(" ");
        var line = "";

        var arr = [];
		this.wordLength = words.length;
        if(words.length == 1){
            for(var n = 0; n < text.length; n++) {
                var testLine = line + text[n];
                var metrics = ctx.measureText(testLine);
                var testWidth = metrics.width;
                if (testWidth > maxWidth && n > 0) {
                    arr.push(line);
                    line = text[n];
                } else {
                    line = testLine;
                }
            }
            arr.push(line);
        }else{
            for(var n = 0; n < words.length; n++) {
                var testLine = line + words[n] + " ";
                var metrics = ctx.measureText(testLine);
                var testWidth = metrics.width;
                if(words[n]=="\n")
                {
                    arr.push(line);
                    line = "";
                }
                else if (testWidth > maxWidth && n > 0) {
                    arr.push(line);
                    line = words[n] + " ";
                }else {
                    line = testLine;
                }
            }
            arr.push(line);
        }
        this.textHeight=(this.textLineHeight+4)*(arr.length);
        
        return arr;
    },

    fillTextWithSpacing:function(context, text, x, y, spacing)
    {
        //Start at position (X, Y).
        //Measure wAll, the width of the entire string using measureText()

        //measure width
        // x=0;
        var widthAll=0;
        context.save();
        context.font = this.textFontSize + "px " + this.textFont;
       
        context.restore();
        for(var i=0;i<text.length-1;i++)
        {
             char = text.substr(i, 1);
             context.save();
             context.font = this.textFontSize + "px " + this.textFont;
             widthAll += context.measureText(char).width;
             context.restore();
        }
        widthAll-=spacing*(text.length+2);

        context.save();
        context.font = this.textFontSize + "px " + this.textFont;
        wAll = context.measureText(text).width;
        if(this.textAlign=="center")
        {
            x-=widthAll/2;
        }
        var widthLine=x;
        var rightSide=0;
        do
        {
        //Remove the first character from the string
        char = text.substr(0, 1);
        text = text.substr(1);
        //Print the first character at position (X, Y) using fillText()
        context.fillText(char, x, y);
        //Measure wShorter, the width of the resulting shorter string using measureText().
        if (text == "")
            wShorter = 0;
        else
            wShorter = context.measureText(text).width;
        //Subtract the width of the shorter string from the width of the entire string, giving the kerned width of the character, wChar = wAll - wShorter
        wChar = wAll - wShorter;
        //Increment X by wChar + spacing
        
        x += wChar -spacing;
        rightSide=x-wChar;
        // context.fillRect(x,y,10,10);
        //wAll = wShorter
        wAll = wShorter;
        //Repeat from step 3
        } while (text != "");
        // context.fillRect(rightSide,y,10,10);
        // context.fillRect(widthAll,y,15,15);
        context.restore();
        // console.log(rightSide+" try "+widthAll);
    },

    drawTextList: function(textList, x, y){
        if(!textList || !textList.length || textList.length <= 0) return;
        var ctx = ig.system.context;
		ctx.font = this.textFontSize + "px " + this.textFont;
        var modY = 0;
        for(var i=0; i<textList.length; i++){

            this.fillTextWithSpacing(ctx,textList[i],x,y+modY,this.textSpacing);
            // ctx.fillText(textList[i], x, y+modY);
            modY += this.textLineHeight+4;
        }

    },
});
});